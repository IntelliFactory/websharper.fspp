﻿namespace proxies

open IntelliFactory.WebSharper

[<Name "ArgumentNullException">]
[<Proxy(typeof<System.ArgumentNullException>)>]
type ArgumentNullExceptionProxy =
    
    [<Inline """new Error("ArgumentNullException")""">]
    new () = {}

    [<Inline """new Error("ArgumentNullException: $paramName")""">]
    new (paramName : string) = {}

[<JavaScript>]
[<Proxy(typeof<System.ArgumentException>)>]
type ArgumentExceptionProxy (message: string) =
    inherit exn(message)

    new () = ArgumentExceptionProxy("ArgumentException")

[<Name "KeyNotFoundException">]
[<Proxy(typeof<System.Collections.Generic.KeyNotFoundException>)>]
type KeyNotFoundExceptionProxy =
    
    [<Inline """new Error("KeyNotFoundException")""">]
    new () = {}

    [<Inline """new Error("KeyNotFoundException : $message")""">]
    new (message : string) = {}