﻿//Deliberately duplicate the proxy, this isn't supposed to work but it does! Change over to new release as soon as it's released.
namespace IntelliFactory.WebSharper

[<IntelliFactory.WebSharper.Core.Attributes.Proxy
    "Microsoft.FSharp.Core.LanguagePrimitives+IntrinsicFunctions, \
     FSharp.Core, Version=2.0.0.0, Culture=neutral, \
     PublicKeyToken=b03f5f7f11d50a3a">]
module IntrinsicFunctionProxy =
    
    [<Inline "$array[$index][$index2]">]
    let GetArray2D (array: 'T[,]) (index: int) (index2: int) = X<'T>

    [<Inline "$array[$index][$index2] = value">]
    let SetArray2D (array: 'T[,]) (index: int) (index2: int) (value : 'T) = ()