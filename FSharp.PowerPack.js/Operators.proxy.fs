﻿//Deliberately duplicate the proxy, this isn't supposed to work but it does! Change over to new release as soon as it's released.
namespace IntelliFactory.WebSharper

[<IntelliFactory.WebSharper.Core.Attributes.Name "Operators">]
[<IntelliFactory.WebSharper.Core.Attributes.Proxy
    "Microsoft.FSharp.Core.Operators, \
     FSharp.Core, Version=2.0.0.0, Culture=neutral, \
     PublicKeyToken=b03f5f7f11d50a3a">]
module OperatorsProxy =

    (*[<Inline "Number($x)">]
    let ToSingle (x: 'T) = X<'T>

    [<Inline "($x << 0)">]
    let ToInt32 (x: 'T) = X<int32>

    [<Inline "$x">]
    let ToEnum<'T when 'T : enum<int>> (x: 'T) = X<'T>*)

    [<JavaScript>]
    let InvalidOp message : 'T =
        failwith message

    [<JavaScript>]
    let InvalidArg (argumentName : string) (message : string) =
        failwith message