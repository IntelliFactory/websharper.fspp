namespace TestBundle

open IntelliFactory.WebSharper
open Microsoft.FSharp.Compatibility.OCaml

#nowarn "62"

[<JavaScript>]
module Client =

    let Main =
        List.rev_append [1..3] [4..6]
        |> Seq.map string
        |> String.concat ", "
        |> JavaScript.Log
