/// <reference path="Content/TestBundle.d.ts" />

import A = IntelliFactory.WebSharper.Arrays;
import L = IntelliFactory.WebSharper.List;
import LX = Microsoft.FSharp.Compatibility.OCaml.ListModule;

function tests() {
    var l1 = L.ofArray([1, 2, 3]);
    var l2 = L.ofArray([4, 5, 6]);
    var l3 = LX.rev_append(l1, l2);
    console.log(A.ofSeq(l3));
}

tests();

